# libs
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

# load data
df= pd.read_csv('https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/edx/project/drinks.csv')
df.head()

# wine servings by continent
wine_by_continent = df[['wine_servings', 'continent']].groupby(['continent'], as_index=False).mean()
print(wine_by_continent)

# describe beer servings
beer_by_continent = df.groupby('continent')['beer_servings']
print(beer_by_continent.describe())

# boxplot for beer_servings
import seaborn as sns
sns.boxplot(x="continent", y="beer_servings", data=df)
plt.xticks(rotation='vertical')
plt.show()

# regplot for wine and beer
# by country
sns.regplot(x='wine_servings', y='beer_servings', data=df)
plt.title('wine vs beer serving by country')
plt.show()
# by continent
wine_beer_df = df.groupby('continent')[['beer_servings', 'wine_servings']].mean()
sns.regplot(x='wine_servings', y='beer_servings', data=wine_beer_df)
plt.title('wine vs beer serving by continent')
plt.show()

# simple linear regression
from sklearn.linear_model import LinearRegression
lm = LinearRegression()

X = df[['wine_servings']]
Y = df['total_litres_of_pure_alcohol']

# fit
lm.fit(X,Y)

print( lm.intercept_ )
print( lm.coef_)

lm.score(X, Y)

# linear regression with full features
# check correlation
df.corr()
# continent as predictor
sns.boxplot(x='continent', y='total_litres_of_pure_alcohol', data=df)
plt.title('Total litres of alcohol by continent')
plt.xticks(rotation='vertical')
plt.show()

# ANOVA test
by_continent = df.groupby(['continent'])
by_continent.describe()['total_litres_of_pure_alcohol']
import scipy.stats as stats
f_val, p_val = stats.f_oneway(by_continent.get_group('Africa')['total_litres_of_pure_alcohol'],
                              by_continent.get_group('Asia')['total_litres_of_pure_alcohol'],
                              by_continent.get_group('Europe')['total_litres_of_pure_alcohol'],
                              by_continent.get_group('North America')['total_litres_of_pure_alcohol'],
                              by_continent.get_group('South America')['total_litres_of_pure_alcohol'],
                              by_continent.get_group('Oceania')['total_litres_of_pure_alcohol'])

print("ANOVA results for continent on total litres of pure alcohol: \nF=", f_val, ", P =", p_val)

# add continent as oneHot variable with dummies
clean_df = pd.concat([df, pd.get_dummies(df['continent'], prefix="continent")], axis=1)
clean_df.drop(columns=['continent'], inplace=True)

# select data for model
X = clean_df.drop(columns=['country', 'total_litres_of_pure_alcohol'], axis=1)
Y = df[['total_litres_of_pure_alcohol']]

# segment data train / test
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=0)

# fit model with train data
mlr = LinearRegression()
mlr.fit(x_train, y_train)

# R^2
print('Model for continent and wine, spirits and beer servings on total litres of pure alcohol')
print( 'intercept: ' + str( mlr.intercept_) + ' coefficients: ' + str( mlr.coef_) )

mlr_coef = pd.concat([pd.DataFrame({'variable':X.columns.tolist(), 'coefficient':mlr.coef_[0].tolist()}),
                      pd.DataFrame({'variable':['intercept'], 'coefficient':mlr.intercept_})], ignore_index=True)
print(mlr_coef)
print('\n Model scores')
print( 'R^2 for train data: ' + str( mlr.score(x_train, y_train) ))
print( 'R^2 for test data: ' + str(mlr.score(x_test, y_test) ))

# create pipeline
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, PolynomialFeatures

transport=[('scale',StandardScaler()), ('polynomial', PolynomialFeatures(include_bias=False)), ('model',LinearRegression())]

pipe=Pipeline(transport)

# fit model with pipeline
pipe.fit(x_train,y_train)

# R^2 score
r2_test = pipe.score(x_test, y_test)
print('R^2 score on pipeline and test data: ' + str(r2_test) )

# predict and score the prediction
yhat = pipe.predict(x_test)
from sklearn.metrics import r2_score
r2_prediction = r2_score(yhat, y_test)
print('R^2 score on pipeline and prediction: ' + str(r2_prediction) )


# Ridge regression
from sklearn.linear_model import Ridge

ridge_m = Ridge(alpha=0.1)
ridge_m.fit(x_train, y_train)

r2_ridge = ridge_m.score(x_test, y_test)
print('R^2 score on Ridge regression ( alfa=0.1 ) and test data: ' + str(r2_ridge) )


# 2nd ord. polynomial and ridge

poly = PolynomialFeatures(degree=2)
x_train_poly = poly.fit_transform(x_train)
x_test_poly = poly.fit_transform(x_test)

# fit model
ridge_poly_lm = Ridge(alpha=0.1)
ridge_poly_lm.fit(x_train_poly, y_train)

# R^2 score on test data
r2_ridge_poly = ridge_poly_lm.score(x_test_poly, y_test)
print('R^2 score on Ridge regression ( alfa=0.1 ) and 2nd order Polynomial on test data: ' + str(r2_ridge_poly) )