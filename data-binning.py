import pandas as pd
import numpy

# read file
cars_data_file_name = 'auto.csv'
cars_df = pd.read_csv(cars_data_file_name)

print( cars_df.head() )

import matplotlib as plt
from matplotlib import pyplot

# print histogram
plt.pyplot.hist(cars_df['horsepower'], bins=5)
# set x/y labels and plot title
plt.pyplot.xlabel("horsepower")
plt.pyplot.ylabel("count")
plt.pyplot.title("horsepower bins")

plt.pyplot.show()

# BINNING
min_hp = cars_df['horsepower'].min()
max_hp = cars_df['horsepower'].max()

bins = numpy.linspace(min_hp, max_hp, 4)
names = ['low', 'mid', 'high']

cars_df['hp-binned'] = pd.cut(cars_df['horsepower'], bins, labels=names, include_lowest=True )

pyplot.bar(names,cars_df['hp-binned'].value_counts())
# set x/y labels and plot title
plt.pyplot.xlabel("horsepower")
plt.pyplot.ylabel("count")
plt.pyplot.title("horsepower bins")
plt.pyplot.show()
"""
# dummy vars
fuel_dummies = pd.get_dummies(cars_df['fuel-type'])
# concatenate frames
cars_df = pd.concat([cars_df, fuel_dummies], axis=1)

# drop fuel-type
cars_df.drop('fuel-type', axis=1, inplace=True)

# write to csv
cars_df.to_csv(cars_data_file_name, index=False)
"""
# value counts for categorical
cars_df['engine-location'].value_counts()

# unique values
cars_df['drive-wheels'].unique()

# grouping
df_group_one = cars_df[['drive-wheels','body-style','price']]
# grouping results
df_group_one = df_group_one.groupby(['drive-wheels'],as_index=False).mean()
df_group_one

# groouping by two variables
# grouping results
df_gptest = cars_df[['drive-wheels','body-style','price']]
grouped_test1 = df_gptest.groupby(['drive-wheels','body-style'],as_index=False).mean()
grouped_test1

# build pivot table
grouped_pivot = grouped_test1.pivot(index='drive-wheels',columns='body-style')
grouped_pivot

# draw it
pyplot.pcolor(grouped_pivot, cmap='RdBu')
pyplot.colorbar()
pyplot.show()

# with proper labels
fig, ax = pyplot.subplots()
im = ax.pcolor(grouped_pivot, cmap='RdBu')

#label names
row_labels = grouped_pivot.columns.levels[1]
col_labels = grouped_pivot.index

#move ticks and labels to the center
ax.set_xticks(numpy.arange(grouped_pivot.shape[1]) + 0.5, minor=False)
ax.set_yticks(numpy.arange(grouped_pivot.shape[0]) + 0.5, minor=False)

#insert labels
ax.set_xticklabels(row_labels, minor=False)
ax.set_yticklabels(col_labels, minor=False)

#rotate label if too long
pyplot.xticks(rotation=90)

fig.colorbar(im)
pyplot.show()

# Pearson correlation coefficient
from scipy import stats

pearson_coef, p_value = stats.pearsonr(cars_df['wheel-base'], cars_df['price'])
print("The Pearson Correlation Coefficient is", pearson_coef, " with a P-value of P =", p_value)

# ANOVA test Analysis of Variance

#group by drive-wheels
grouped_test2 = cars_df[['drive-wheels', 'price']].groupby(['drive-wheels'])
grouped_test2.get_group('4wd')['price']

# ANOVA of drive-wheels vs price
f_val, p_val = stats.f_oneway(grouped_test2.get_group('fwd')['price'], grouped_test2.get_group('rwd')['price'],
                              grouped_test2.get_group('4wd')['price'])

print("ANOVA results: F=", f_val, ", P =", p_val)

# separately 4wd and rwd
f_val, p_val = stats.f_oneway(grouped_test2.get_group('4wd')['price'], grouped_test2.get_group('rwd')['price'])

print("ANOVA results: F=", f_val, ", P =", p_val)

# separately rwd and fwd
f_val, p_val = stats.f_oneway(grouped_test2.get_group('rwd')['price'], grouped_test2.get_group('fwd')['price'])

print("ANOVA results: F=", f_val, ", P =", p_val)

# separately 4wd and fwd
f_val, p_val = stats.f_oneway(grouped_test2.get_group('4wd')['price'], grouped_test2.get_group('fwd')['price'])

print("ANOVA results: F=", f_val, ", P =", p_val)

