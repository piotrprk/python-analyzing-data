# import pandas library
import pandas as pd
from pathlib2 import Path


# read file
cars_data_file_name = 'auto.csv'
cars_data_path = "https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/" + \
                 cars_data_file_name
headers = ["symboling","normalized-losses","make","fuel-type","aspiration", "num-of-doors","body-style",
         "drive-wheels","engine-location","wheel-base", "length","width","height","curb-weight","engine-type",
         "num-of-cylinders", "engine-size","fuel-system","bore","stroke","compression-ratio","horsepower",
         "peak-rpm","city-mpg","highway-mpg","price"]

cars_df = pd.DataFrame()
if Path(cars_data_file_name).is_file():
    cars_df = pd.read_csv(cars_data_file_name)
else:
    cars_df = pd.read_csv(cars_data_path, names=headers)

#cars_df = pd.read_csv(cars_data_path, header=None)
print(cars_df.head())


# create headers list
print(cars_df.columns)

""""
if type(cars_df.columns[0]) != str:
    cars_df.columns = headers
    print(cars_df.columns)
"""
#replace ? with NaN
import numpy as np
cars_df.replace("?", np.nan, inplace=True)

# count missing values
missing = cars_df.isnull()
for col in missing.columns:
    print(col)
    print(missing[col].value_counts())

# simply drop whole row with NaN in "price" column
cars_df.dropna(subset=["price"], axis=0, inplace=True)

# reset index, because we droped two rows
cars_df.reset_index(drop=True, inplace=True)

# replace NaN with average for numeric columns
for col in cars_df.columns:
    if cars_df[col].dtype in ['int64', 'float64']:
        avg = cars_df[col].astype('float').mean(axis=0)
        cars_df[col].replace(np.nan, avg, inplace=True)

# replace NaN with most frequent for categorical
for col in cars_df.columns:
    if cars_df[col].dtype == 'object' and col != 'price':
        max_val = cars_df[col].value_counts().idxmax()
        cars_df[col].replace(np.nan, max_val, inplace=True)

# bind datatypes
cars_df['horsepower'] = cars_df['horsepower'].astype('int')
cars_df[["bore", "stroke"]] = cars_df[["bore", "stroke"]].astype("float")
cars_df[["normalized-losses"]] = cars_df[["normalized-losses"]].astype("int")
cars_df[["price"]] = cars_df[["price"]].astype("float")
cars_df[["peak-rpm"]] = cars_df[["peak-rpm"]].astype("float")

# write to csv
cars_df.to_csv(cars_data_file_name, index=False)


