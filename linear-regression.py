# import libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# read file
cars_data_file_name = 'auto.csv'
cars_df = pd.read_csv(cars_data_file_name)

print( cars_df.head() )

# simple regression highway-mpg vs price
from sklearn.linear_model import LinearRegression
lm = LinearRegression()

X = cars_df[['highway-mpg']]
Y = cars_df['price']

# fit
lm.fit(X,Y)

print( lm.intercept_ )
print( lm.coef_)
# predict

Yhat=lm.predict(X)
print( Yhat[0:5] )

# MSE
from sklearn.metrics import mean_squared_error
mse = mean_squared_error(cars_df['price'], Yhat)
print('The mean square error of price and predicted value is: ', mse)

# Multiple linear regression MLR
# 1. fit thr model
Z = cars_df[['horsepower', 'curb-weight', 'engine-size', 'highway-mpg']]
lm.fit(Z, cars_df['price'])

print( lm.intercept_, lm.coef_ )

# 2. predict
Yhat = lm.predict(Z)

# 3. MSE
mse = mean_squared_error(cars_df['price'], Yhat)
print('The mean square error of price and predicted value is: ', mse)

# visualization
import seaborn as sns
import matplotlib.pyplot as plt

# regression plot
width = 8
height = 6
plt.figure(figsize=(width, height))
sns.regplot(x="highway-mpg", y="price", data=cars_df)
plt.ylim(0,)
plt.show()

plt.figure(figsize=(width, height))
sns.regplot(x="peak-rpm", y="price", data=cars_df)
plt.ylim(0,)

# what's the correlation ?
cars_df[['peak-rpm', 'highway-mpg', 'price']].corr()

# residual plot
plt.figure(figsize=(width, height))
sns.residplot(cars_df['highway-mpg'], cars_df['price'])
plt.show()

# distribution plot values vs fitted
plt.figure(figsize=(width, height))
ax1 = sns.distplot(cars_df['price'], hist=False, color="r", label="Actual Value")
plt = sns.distplot(Yhat, hist=False, color="b", label="Fitted Values", ax=ax1)

plt.title('Actual vs Fitted Values for Price')
plt.xlabel('Price (in dollars)')
plt.ylabel('Proportion of Cars')
plt.show()
plt.close()

# polynomial linear regression MLR

# define function to plot data


def PlotPolly(model, independent_variable, dependent_variabble, Name):
    x_new = np.linspace(15, 55, 100)
    y_new = model(x_new)

    plt.plot(independent_variable, dependent_variabble, '.', x_new, y_new, '-')
    plt.title('Polynomial Fit with Matplotlib for Price ~ Length')
    ax = plt.gca()
    ax.set_facecolor((0.898, 0.898, 0.898))
    fig = plt.gcf()
    plt.xlabel(Name)
    plt.ylabel('Price of Cars')

    plt.show()
    plt.close()


import numpy as np

x = cars_df['highway-mpg']
y = cars_df['price']
f = np.polyfit(x, y, 3)
p = np.poly1d(f)
print(p)
PlotPolly(p, x, y, 'highway-mpg')

# We can perform a polynomial transform on multiple features
from sklearn.preprocessing import PolynomialFeatures

pr=PolynomialFeatures(degree=2)
pr

Z_pr=pr.fit_transform(Z)

Z.shape
Z_pr.shape

# pipeline

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import PolynomialFeatures

Input=[('scale',StandardScaler()), ('polynomial', PolynomialFeatures(include_bias=False)), ('model',LinearRegression())]
pipe=Pipeline(Input)

# fit model with pipeline
pipe.fit(Z,y)

# predict
ypipe=pipe.predict(Z)
print( ypipe[0:9] )

# R-squared
lm.score(Z, Y, )
