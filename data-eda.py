import pandas as pd
import numpy as np

# read file
cars_data_file_name = 'auto.csv'
cars_df = pd.read_csv(cars_data_file_name)

print( cars_df.head() )

# import matplot lib
import matplotlib.pyplot as plt
# import seaborn
import seaborn as sns

# correlation table
print( cars_df.corr() )

# Engine size as potential predictor variable of price
cars_df[['engine-size', 'price']].corr()
sns.regplot(x="engine-size", y="price", data=cars_df)
plt.ylim(0,)
plt.show()

# stroke vs price
sns.regplot(x='stroke', y='price', data=cars_df)
plt.show()

# boxplot for categorical vars
sns.boxplot(x="body-style", y="price", data=cars_df)

# drive-wheels
sns.boxplot(x="drive-wheels", y="price", data=cars_df)

# describe function
cars_df.describe()
# describe for object variables (categorical)
cars_df.describe(include=['object'])